import pandas as pd
import numpy as np

def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    titles = [" Mr. ", " Mrs. ", " Miss. "]
    def list_median(num_list):
        if num_list.notnull().any():
            return round(num_list.median())
        else:
            return np.nan
    def list_missing(num_list):
        return num_list.isnull().sum()
    df["Title"] = np.nan
    for title in titles:
        mask = df['Name'].str.contains(title, case = False, na = False)
        df.loc[mask, "Title"] = title
    df["Title"].fillna("No title", inplace = True)

    results = df.groupby("Title")["Age"].agg([list_missing,list_median]).reset_index()
    results.columns = ["Title", "Missing Values", "Median Age"]
    results["Title"] = results["Title"].str.strip()
    list_of_tuples = [tuple(row) for row in results.itertuples(index = False, name = None)]
    new_list = list(list_of_tuples[1], list_of_tuples[2], list_of_tuples[0])
    return new_list
